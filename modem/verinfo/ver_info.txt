{
    "Image_Build_IDs": {
        "adsp": "ADSP.VT.5.4.2-00366-MANNAR-1", 
        "apps": "LA.UM.9.16.r1-07800.01-MANNAR.QSSI12.0-1", 
        "boot": "BOOT.XF.4.2-00248-MANNARLAZ-1", 
        "btfm": "BTFM.CHE.3.2.1-00257-QCACHROMZ-1", 
        "btfm_CHK": "BTFM.CHE.2.1.5-00284-QCACHROMZ-1", 
        "cdsp": "CDSP.VT.2.4.2-00297-MANNAR-1", 
        "common": "Mannar.LA.2.0-00030-STD.PROD-1", 
        "glue": "GLUE.MANNAR_LA.2.0-00006-NOOP_TEST-1", 
        "modem": "MPSS.HI.4.3.1-01086.1-MANNAR_GEN_PACK-1", 
        "rpm": "RPM.BF.1.11-00131-MANNARAAAAANAZR-2", 
        "tz": "TZ.XF.5.1-01070-MANNARAAAAANAZT-1", 
        "tz_apps": "TZ.APPS.2.0-00199-MANNARAAAAANAZT-1", 
        "video": "VIDEO.VE.6.0-00049-PROD-1", 
        "wlan": "WLAN.HL.3.3.2-00445-QCAHLSWMTPLZ-1"
    }, 
    "Metabuild_Info": {
        "Meta_Build_ID": "Mannar.LA.2.0-00030-STD.PROD-1", 
        "Product_Flavor": "asic", 
        "Time_Stamp": "2022-03-25 14:29:39"
    }, 
    "Version": "1.0"
}